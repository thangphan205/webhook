from fastapi import APIRouter, Header, Request, Response
from pydantic import BaseModel
from loguru import logger
import hmac
from transport.telegram import telegram_bot_sendtext
from pprint import pprint


router = APIRouter()


class WebhookResponse(BaseModel):
    result: str


class WebhookData(BaseModel):

    sid: str = ""
    search_name: str = ""
    app: str = ""
    owner: str = ""
    results_link: str = ""
    result: dict = ""


WEBHOOK_SECRET = "My precious"


@router.post("/")
async def webhook_coming(
    webhook_input: dict,
    request: Request,
    response: Response,
    content_length: int = 0,
    x_hook_signature: str = Header(None)
):
    if content_length > 1_000_000:
        # To prevent memory allocation attacks
        logger.error(f"Content too long ({content_length})")
        response.status_code = 400
        return {"result": "Content too long"}
    if x_hook_signature:
        raw_input = await request.body()
        input_hmac = hmac.new(
            key=WEBHOOK_SECRET.encode(),
            msg=raw_input,
            digestmod="sha512"
        )
        if not hmac.compare_digest(
                input_hmac.hexdigest(),
                x_hook_signature
        ):
            logger.error("Invalid message signature")
            response.status_code = 400
            return {"result": "Invalid message signature"}
        logger.info("Message signature checked ok")
    else:
        logger.info("No message signature to check")

    # logger.info(f"Raw data: {webhook_input}")
    logger.info(f"RESULTS:")
    pprint(webhook_input["result"])
    data_alert = "{} - {} - {}".format(webhook_input["result"]["tddmo_cidr"],
                                       webhook_input["result"]["tddmo_description"], webhook_input["result"]["text"])
    telegram_bot_sendtext(data_alert)
    return {"result": webhook_input["result"]}
